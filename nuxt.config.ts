export default defineNuxtConfig({
  ssr: false,
  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },
  modules: [
    "@pinia/nuxt",
    "@nuxtjs/i18n",
    "@vueuse/nuxt",
    "@bootstrap-vue-next/nuxt",
    "@nuxtjs/eslint-module",
    "floating-vue/nuxt",
  ],
  i18n: {
    vueI18n: "./i18n.config.ts",
  },
  app: {
    pageTransition: {
      name: "change-page",
      mode: "out-in",
    },
  },
  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL,
    },
  },
});
