FROM node:22 AS build

WORKDIR /build
COPY . .

RUN apt update && apt install -y curl

RUN curl -fsSL https://bun.sh/install | BUN_INSTALL=/usr bash

RUN bun install

RUN NITRO_PRESET=bun bun --node nuxi build

RUN rm -rf .output/server/node_modules

RUN cd .output/server && \
    bun install

FROM oven/bun:1-distroless

WORKDIR /app
COPY --from=build /build/.output .

EXPOSE 3000
CMD [ "./server/index.mjs" ]
