import useUserStore from "~/store/user";

export default defineNuxtRouteMiddleware((to) => {
  if (to.path === "/setup") {
    return;
  }
  const userStore = useUserStore();
  if (to.path !== "/" && !userStore.jwt) {
    return navigateTo("/");
  }
  if (to.path === "/" && userStore.jwt) {
    return navigateTo("/dashboard");
  }
});
