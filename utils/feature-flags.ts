// Enable HTTPS-related features
export const ENABLE_HTTPS = true;
// Enable opening channels from the dashboard
export const ENABLE_CHANNEL_OPEN = true;
// Show the button to send on-chain sats
export const ENABLE_ONCHAIN_TX = true;
// Allow to upgrade to Nirvati
export const ENABLE_NIRVATI = true;
